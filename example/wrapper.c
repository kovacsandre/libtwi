#include <avr/io.h>
#include "twi/twi.h"
#include "wrapper.h"

uint8_t
MCP23017_writeByte(uint8_t address, uint8_t reg, uint8_t data)
{
    uint8_t adata[] = { reg, data };
    uint8_t *rdata = adata;

    return twi_writeTo(address, rdata, 2, 1, 1);
}

uint8_t
MCP23017_readByte(uint8_t address, uint8_t reg, uint8_t *data)
{
    twi_writeTo(address, &reg, 1, 1, 1);

    return twi_readFrom(address, data, 1, 1);
}
