#ifndef WRAPPER_H
#define WRAPPER_H

uint8_t MCP23017_writeByte(uint8_t address, uint8_t reg, uint8_t data);
uint8_t MCP23017_readByte(uint8_t address, uint8_t reg, uint8_t *data);

#endif
