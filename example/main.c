#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "uart/uart.h"
#include "twi/twi.h"
#include "wrapper.h"
#include "main.h"

#define I2C_ADDR  0x20      // A0 A1 A2 connected to GND (0x40 << 1)

int
main(void)
{
    uint8_t data = 0x01;

    /* Init UART0 and I2C at 400 kHz */
    USART0_Init();
    twi_setFrequency(400000);
    twi_init();
    sei();

    /* Set IOCON.BANK to zero (see datasheet table 3-4 and 3-5) */
    MCP23017_writeByte(I2C_ADDR, 0x05, 0x00);
    /* Set PortA direction */
    MCP23017_writeByte(I2C_ADDR, MCP23017_IODIRA, 0xFF);
    /* Set pull-ups for PortA */
    MCP23017_writeByte(I2C_ADDR, MCP23017_GPPUA, 0xFF);

    for(;;) {
        /* Read PortA data and write to UART0
         * (normally you do this when an interrupt occus from the IC)
         */
        MCP23017_readByte(I2C_ADDR, MCP23017_GPIOA, &data);
        USART0_Transmit(data);
        /* 1s Delay */
        _delay_ms(1000);
    }

    return 0;
}
