TARGET = twi
MCU = at90can32
F_CPU = 16000000
CFLAGS = -D F_CPU=$(F_CPU) -mmcu=$(MCU) -Os -funsigned-char -Wall

%.a: %.o
	avr-ar ru $(TARGET).a $< $@

%.o: %.c
	avr-gcc -c $(CFLAGS) $< -o $@

all: $(TARGET).a

clean:
	rm -f *.o
	rm -f *.a

.PHONY: all clean
